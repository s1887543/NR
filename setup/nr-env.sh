#!/bin/bash


# Python 2 
virtualenv -p python2 /scratch/nr-env
source /scratch/nr-env/bin/activate
pip install --upgrade pip
pip install pandas matplotlib scipy sklearn pydotplus seaborn tensorflow ipython ipykernel 
ipython kernel install --user --name=nr-env
deactivate

