{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from sklearn import datasets, tree, metrics, model_selection, ensemble\n",
    "from sklearn.externals.six import StringIO \n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.colors as colors\n",
    "from IPython.display import Image\n",
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "import pydotplus\n",
    "import itertools\n",
    "import seaborn as sns\n",
    "import dt_utils\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Notebook notes\n",
    "\n",
    "- I have provided a number of **utility functions** to help with the visualisation and repeated application of the machine learning methods below. Although not required, I recommend inspecting the code in `dt_utils.py` so that you have a good understanding of how the plots are produced and configured. Feel free to modify and to tune any of the settings as you wish. This may not be totally bug free so please let me know if you spot any problems\n",
    "- A number of the imported libraries may not be available on your laptop/desktop PC by default. I have requested that these are available on the Lab PCs by Monday (I'll provide alternative instructions if this cannot be done)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Iris Dataset\n",
    "\n",
    "The *Iris dataset* is a standard \"toy\" dataset useful for evaluating and comparing machine learning techniques. This data consists of three different types of irises’ (*Setosa*, *Versicolour*, and *Virginica*) and comprises of **150** observations with **4** features per observation (*Sepal Length*, *Sepal Width*, *Petal Length* and *Petal Width*).\n",
    "\n",
    "This dataset is available within **scikit-learn** (see [The Iris Dataset](http://scikit-learn.org/stable/auto_examples/datasets/plot_iris_dataset.html)) and can be access using the `datasets` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load iris dataset \n",
    "iris = datasets.load_iris()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Show dataset dimensions\n",
    "iris.data.shape, iris.target.shape, np.unique(iris.target)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Show feature names\n",
    "iris.feature_names"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display sample entry\n",
    "iris.data[0, [1,2]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before applying any machine learning technique it is first useful to visually explore the data to investigate the separation of classes in feature space. For the iris dataset we can decompose the feature space of 4 variables into 6 x 2-dimensional plots showing the distribution for each pair of features: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iris.target_names"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate 2D plots \n",
    "dt_utils.irisplot(iris.data, iris.target, 3,\n",
    "             t_names=iris.feature_names, \n",
    "             c_names=iris.target_names)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly a 3D plot can show class distribution across 3 of the 4 input features:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 3D plot\n",
    "fig = plt.figure(1, figsize=(6, 4))\n",
    "ax = Axes3D(fig, elev=-150, azim=110)\n",
    "ax.scatter(iris.data[:, 0], iris.data[:, 1], iris.data[:, 3], c=iris.target,\n",
    "           cmap=plt.cm.Set1, edgecolor='k', s=40)\n",
    "ax.set_title(\"Iris features\")\n",
    "ax.set_xlabel(\"Sepal length\")\n",
    "ax.set_ylabel(\"Sepal width\")\n",
    "ax.set_zlabel(\"Petal length\")\n",
    "ax.w_xaxis.set_ticklabels([])\n",
    "ax.w_yaxis.set_ticklabels([])\n",
    "ax.w_zaxis.set_ticklabels([])\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see in each of the plots of the above that the *Setosa* iris (red) is linearly separable from the other two classes. It is less trivial to linearly seperate the *Versicolour* and *Virginica* irises based on the provided feature information. Lets see if a decision tree created by learning from the dataset can help. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fit and Predict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In scikit-learn it is very simple to apply a machine learning technique (or *estimator*) to a given dataset. First define the estimator you wish to apply - in our case this is the `DecisionTreeClassifier`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define classifier \n",
    "clf = tree.DecisionTreeClassifier()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that this is accessed by via the `sklearn.tree` module which was imported at the top of the notebook.\n",
    "\n",
    "We now *fit* the data to the chosen estimator. All estimators expect the data to be presented in the same form: \n",
    "- For the *data*: One row per observation and one column per feature\n",
    "- For the *target*: One row per class result\n",
    "\n",
    "In this case the *data* is the 150 observations of 4 features (150 x 4) array and the *target* is the observed result (150 x 1) (i.e. the type of iris) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "# apply fit to training data\n",
    "fit = clf.fit(iris.data, iris.target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that above with have used the IPython magic command `%%time` to measure the execution time of the fit. Measuring the execution time will be more important as we fit larger datasets and run paramater scans and ensemble methods. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Evaluation\n",
    "\n",
    "Now that we have performed the fitting step we can look at the generated tree in more detail. *Scikit-learn* provides a method to visualise the decision made at each branch of the tree:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot decision tree\n",
    "dt_utils.plotDT(fit, iris.feature_names, iris.target_names, fname='iris.dot') # plots to file (run \"dot -Tpng iris.dot -o tree.png\")\n",
    "#graph = dt_utils.plotDT(fit, iris.feature_names, iris.target_names)\n",
    "#Image(graph.create_png())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that as expected the *setosa* iris was easily seperated with a single condition able to isolate all observations (petal length < 2.45cm)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next lets plot a **decision surface** to show how the classification results are applied across all the feature space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = tree.DecisionTreeClassifier()\n",
    "dt_utils.irisplot(iris.data, iris.target, 3, \n",
    "            clf=clf, \n",
    "            t_names=iris.feature_names, \n",
    "            c_names=iris.target_names)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By inspection you can see that the decision tree performs very well indeed.\n",
    "\n",
    "But what we have actually done here? Without any *additional* observations we are unable to determine how the estimator performs - we only know how it performs against our *training* data. \n",
    "\n",
    "Looking at the top left plot above we see that the decision surface in the centre of the plot is highly fragmented into versicolor and virginica (yellow and blue) choices. This appears to be **overfitting** the decision tree to the available data and is unlikely to generalise well to unknown additional observations. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training and Testing Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To develop a more general and realistic model we need partition the data in a **training** and **testing** sample.\n",
    "\n",
    "There are many ways to split this data. First we can just use *array slicing*. For example we can hold out the last 10 observations (and results) for testing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Split through inplace order (last 10 for testing) \n",
    "train_data = iris.data[:-10]\n",
    "train_target = iris.target[:-10]\n",
    "test_data = iris.data[-10:]\n",
    "test_target = iris.target[-10:]\n",
    "\n",
    "# check shapes\n",
    "print(train_data.shape, train_target.shape, test_data.shape, test_target.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general this method is not advised as there may be sequence (or time) dependent attributes in the data. For the iris dataset this is unimportant (the type of iris is unlikely to be affected by observation order *unless* the dataset is explictly ordered by iris type) but it is useful to show how to remove this using a *random permutation*:   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# splitting training and testing using random permutation, test on last 10 entries \n",
    "\n",
    "np.random.seed(0)\n",
    "indices = np.random.permutation(len(iris.data))\n",
    "\n",
    "# show permutation \n",
    "print(indices[:10])\n",
    "\n",
    "train_data = iris.data[indices[:-10]]\n",
    "train_target = iris.target[indices[:-10]]\n",
    "test_data = iris.data[indices[-10:]]\n",
    "test_target = iris.target[indices[-10:]]\n",
    "\n",
    "print(train_data.shape, train_target.shape, test_data.shape, test_target.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most convenient approach when using *scikit-learn* is to use the inbuilt `model_selection` method `train_test_split`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# train test split \n",
    "train_data, test_data, train_target, test_target = model_selection.train_test_split(\n",
    "    iris.data, iris.target, test_size=0.7, random_state=0)\n",
    "\n",
    "print(train_data.shape, train_target.shape, test_data.shape, test_target.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here only 30% of the data has been assigned for estimator training and the rest for testing. Feel free to modify the parameters above to generate a different training and testing split.\n",
    "\n",
    "Now let us run the classification again but this time just using the training data: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# re-run classification\n",
    "clf = tree.DecisionTreeClassifier()\n",
    "fit = clf.fit(train_data, train_target)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now use the classifier to *predict* iris types using the testing data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define expected and predicted \n",
    "expected = test_target\n",
    "predicted = clf.predict(test_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets look at the decision tree for this new fit:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot tree \n",
    "graph = dt_utils.plotDT(fit, iris.feature_names, iris.target_names)\n",
    "Image(graph.create_png())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the much reduced training sample the tree has much depth and far fewer branches than before. The decision surface is also less complex:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# re-plot decision surfaces \n",
    "dt_utils.irisplot(train_data, train_target, 3, \n",
    "            clf=clf, \n",
    "            d_test=test_data,\n",
    "            t_test=test_target,\n",
    "            t_names=iris.feature_names, \n",
    "            c_names=iris.target_names)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Performance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now need to quantify how well the model fits the testing data. This can be provided by *scikit-learn* in a number of ways. To get an overview of performance we can first run a **clasification report**: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display classification report \n",
    "d = model_selection.train_test_split(iris.data, iris.target, test_size=0.7, random_state=0)\n",
    "expected, predicted = dt_utils.runML(tree.DecisionTreeClassifier(), d)\n",
    "report = metrics.classification_report(expected, predicted, target_names=iris.target_names)\n",
    "print(report)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In a binary classification context the **precision** is the ratio of *true positives* to the sum of *true positives* and *false positives*. The **recall** is the ratio of *true positives* to the sum of *true positives* and *false negatives*. The **support** column shows the number of samples per class provided by the training data. This is a useful metric to determine any *class imbalances*.\n",
    "\n",
    "Other performance metrics such as the classification *accuracy* can be retireved directly from the `metrics` module. This provides a single score of the fraction of the correct predictions across all classes: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# accuracy score \n",
    "acc = metrics.accuracy_score(expected, predicted)\n",
    "print(acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The distribution of false positives and negatives can be illustrated using a **confusion matrix**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display confusion matrix\n",
    "cm = metrics.confusion_matrix(expected, predicted)\n",
    "print(cm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Diagnonal terms are the number of true positives for each class (per row). Off-diagonal terms in the same row indiciate false negatives and the same column false positives. \n",
    "\n",
    "A confusion matrix can be better illustrated as *heatmaps* using the following methods: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot confusion matrix \n",
    "dt_utils.plot_cm(cm, iris.target_names)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# normalized confusion matrix\n",
    "dt_utils.plot_cm(cm, iris.target_names, normalize=True)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# alternative using seaborn library\n",
    "dt_utils.heatmap(cm, labels=['Predicted', 'True'], \n",
    "        classes=[iris.target_names,iris.target_names],\n",
    "        normalize=False)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tuning Estimator Hyper-parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each estimator provided by *scikit-learn* has a number of associated **hyper-parameters** that tune the behaviour of their learning approach.\n",
    "\n",
    "For the `DecisionTreeClassifier`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = tree.DecisionTreeClassifier()\n",
    "clf.get_params()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All hyper-parameters provided by an estimator can have their default values modified when creating an estimator instance. For example we can run the decision tree classification again but limit the maximum depth of the tree to 2:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# DT with maximum depth = 2 \n",
    "d = model_selection.train_test_split(iris.data, iris.target, test_size=0.3, random_state=0)\n",
    "clf = tree.DecisionTreeClassifier(max_depth=2)\n",
    "e, p = dt_utils.runML(clf, d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we use the convenience method `runML` to re-run the fitting and prediction steps highlighted earlier on in the notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are many hyper-parameters we could tune and in general non-default values should be explored in order to get better accuracy, precision and recall metrics. \n",
    "\n",
    "A **Grid search** method (`GridSearchCV`) can be used to easily perform a hyper-parameter scan for a given estimator in order to determine the best choice of values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "# adapted from http://scikit-learn.org/stable/auto_examples/model_selection/plot_grid_search_digits.html#sphx-glr-auto-examples-model-selection-plot-grid-search-digits-py\n",
    "\n",
    "train_data, test_data, train_target, test_target = model_selection.train_test_split(\n",
    "    iris.data, iris.target, test_size=0.9, random_state=0)\n",
    "\n",
    "params = {'max_leaf_nodes' : [2, 3, 4, 5, 10], \n",
    "          'max_depth' : [2, 3, 4, 5]\n",
    "         }\n",
    "score = 'accuracy'\n",
    "clf = model_selection.GridSearchCV(tree.DecisionTreeClassifier(), params, cv=5, scoring='%s' % score)\n",
    "clf.fit(train_data, train_target)\n",
    "\n",
    "print(clf.best_params_)\n",
    "means = clf.cv_results_['mean_test_score']\n",
    "stds = clf.cv_results_['std_test_score']\n",
    "\n",
    "# uncomment to show explicit list of means/std\n",
    "# for mean, std, param in zip(means, stds, clf.cv_results_['params']):\n",
    "#         print(\"%0.3f (+/-%0.03f) for %r\"\n",
    "#               % (mean, std * 2, param))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The mean accuracy for each choice of hyper-parameters can be illustrated using a heatmap:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# heatmap (only works on 2D grid search!)\n",
    "labels = [k for k,v in params.items()]\n",
    "classes = [v for k,v in params.items()]\n",
    "dt_utils.heatmap(means.reshape(4,5), classes=classes, labels=labels, palette=\"Red\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have provided multiple values each for the *maximum number of leaf nodes* and the *maximum depth of the tree*. `GridSearchCV` applies each combination of values in turn when the `fit` method is applied. \n",
    "\n",
    "The best set of hyper-paramters is stored in `best_params_`. Accuracy scores for each combination can also be access through the `fit` instance.\n",
    "\n",
    "Note that a grid search could take a long time to execute depending on: \n",
    "- the size of the training sample \n",
    "- the number of hyper-paramaters combinations\n",
    "- the choice of estimator \n",
    "- the choice of hyper-parameters\n",
    "\n",
    "It is not recommended to perform a \"blind\" search across all hyper-parameter space. In most cases the defaults provided by *scikit-learn* usually give good performance. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cross Validation "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the hyper-parameter scan above we could have used the whole dataset as  a larger sample to determine the best choices for the estimator. However this again would have led to overfitting the data even if we split the data into training and testing sets after the choice had been determined. \n",
    "\n",
    "Ideally it therefore makes sense to partition the data into three sets with the third \"validation\" set isolated from the training and optimisation steps. This may not be possible where data is limited and where performance is sensitive to the loss of two-thirds of the dataset. Instead, **cross-validation** can be used to create multiple hold-out sets (see the [scikit-learn user guide](http://scikit-learn.org/stable/modules/cross_validation.html) for a fuller explanation). \n",
    "\n",
    "We can illustrate some of the paritioning schemes using a simple 10 item array: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# kfold examples \n",
    "kf = model_selection.KFold(n_splits=4)\n",
    "for train, test in kf.split(np.arange(10)):\n",
    "    print(\"%s %s\" % (train, test))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# shuffle split example\n",
    "ss = model_selection.ShuffleSplit(n_splits=5, test_size=0.25, random_state=0)\n",
    "for train, test in ss.split(np.arange(10)):\n",
    "    print(\"%s %s\" % (train, test))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets run a cross validation on the Decision Tree with 5 folds on the original dataset: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cross validation \n",
    "clf = tree.DecisionTreeClassifier()\n",
    "scores = model_selection.cross_val_score(clf, iris.data, iris.target, cv=5)\n",
    "print(scores)\n",
    "print(\"Accuracy: %0.2f (+/- %0.2f)\" % (scores.mean(), scores.std() * 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly with the shuffle split approach:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# alternative with shuffle split\n",
    "cv = model_selection.ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)\n",
    "scores = model_selection.cross_val_score(clf, iris.data, iris.target, cv=cv)\n",
    "\n",
    "print(scores)\n",
    "print(\"Accuracy: %0.2f (+/- %0.2f)\" % (scores.mean(), scores.std() * 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ensemble Methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our single decision tree performs incredibly well on the iris dataset with no further optimisation needed. In general the performance can be improved by considering an **ensemble** (or **random forest**) of trees. \n",
    "\n",
    "See the lecture and the [*Scikit-learn* user guide](http://scikit-learn.org/stable/modules/ensemble.html) for more details. \n",
    "\n",
    "This is accomplished easily in *scikit-learn* using the `RandomForestClassifier` method in the`ensemble` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = ensemble.RandomForestClassifier(n_estimators=10, max_features=2)\n",
    "d = model_selection.train_test_split(iris.data, iris.target, test_size=0.3, random_state=0)\n",
    "e, p = dt_utils.runML(clf, d)\n",
    "report = metrics.classification_report(e, p)\n",
    "print(report)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A grid search can also be performed on the ensemble method to find the best number of trees to combine: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "params = {'n_estimators' : [2, 5, 10, 50, 100], \n",
    "          'max_features' : [2, 3, 4]\n",
    "         }\n",
    "score = 'accuracy'\n",
    "clf = model_selection.GridSearchCV(ensemble.RandomForestClassifier(), params, cv=5, scoring='%s' % score)\n",
    "clf.fit(train_data, train_target)\n",
    "means = clf.cv_results_['mean_test_score']\n",
    "labels = [k for k,v in params.items()]\n",
    "classes = [v for k,v in params.items()]\n",
    "dt_utils.heatmap(means.reshape(3,5), classes=classes, labels=labels, palette=\"Red\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
